# Portuguese translation of isa-support's package debconf messages
# Copyright (C) 2017 THE isa-support'S COPYRIGHT HOLDER
# This file is distributed under the same license as the isa-support package.
# Rui Branco <ruipb@debianpt.org>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: isa-support 1\n"
"Report-Msgid-Bugs-To: isa-support@packages.debian.org\n"
"POT-Creation-Date: 2022-08-17 11:25+0000\n"
"PO-Revision-Date: 2017-08-09 10:52+0100\n"
"Last-Translator: Rui Branco <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: note
#. Description
#: ../@NAME@-support.templates.in:1001
msgid "Support for @NAME@ required"
msgstr "Necessário suporte para @NAME@"

#. Type: note
#. Description
#: ../@NAME@-support.templates.in:1001
msgid ""
"Alas, your machine doesn't support the @NAME@ instruction set.  It is needed "
"by software that depends on this dummy package.  Sorry."
msgstr ""
"Infelizmente a sua máquina não suporta o conjunto de instruções @NAME@. O "
"mesmo é necessário para o software que depende deste pacote fictício.  "
"Lamentamos."

#. Type: note
#. Description
#: ../@NAME@-support.templates.in:1001
msgid "Aborting installation."
msgstr "A abortar a instalação."
